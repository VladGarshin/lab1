function swap(first_item, second_item) {
    let temp = first_item;      
    first_item = second_item;
    second_item = temp; 
}
function bubble_sort(arr){ 
    for (var i = 0; i < arr.length; ++i) {
        for(let j = 0; j < arr.length - i - 1; j++){ 
            if(arr[j] > arr[j+1]){
                swap(arr[j], arr[j+1]);
            }}}
            return arr; }
console.log(bubble_sort([5,4,3,2,1]));
